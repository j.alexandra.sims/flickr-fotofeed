$(document).ready(function () {
  $('button').click(function () {
    $('button').removeClass('selected'); // removes the 'selected' class from the button that was previously selected
    $(this).addClass('selected'); // 'this' refers to only the button the user clicks.
    var flickrAPIURL = 'https://api.flickr.com/services/feeds/photos_public.gne?jsoncallback=?';
    var animal = $(this).text().toLowerCase(); // 'this' still refers to the button the visitor clicks.
    console.log(animal);
    var flickrOptions = { 
      tags : animal,
      format : 'json'
    };
    console.log(flickrOptions);
    // 'data' represents the json data returned by jQuery. jQuery parsed the return JSON string from the server so that the data is Actual JavaScript at this point.
    function displayPhotos(data) {
      var photoHTML = '<ul>';
      $.each( data.items, function ( i, photo ) {
        // build up the HTML for each photo
        photoHTML += '<li class="grid-25 tablet-grid-50">';
        photoHTML += '<a href="' + photo.link + '" class="image">';
        photoHTML += '<img src="' + photo.media.m + '"</a></li>';
      });
      photoHTML += '</ul>';
      $('#photos').html(photoHTML);
    };
    
    $.getJSON(flickrAPIURL, flickrOptions, displayPhotos);
  });
});  // end ready